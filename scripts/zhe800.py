#/usr/bin/python
#coding:utf-8

from wishing import models
from contrib import qiniu_util
import time, datetime
import random, re
import json
import requests
import urllib
from decimal import Decimal

CURRENT_SOURCE = 3
PATTERN = re.compile('i(\d*)\.htm')

IOS_UA = 'tbbz|tao800|9adc0955a916c83a2707a50221cfdccbd5407eb9|iPhone|4.0.2|b2e1cc'

CATE_MAP = {
        #女装
        1024: (9, 220),#裙装
        1023: (9, 0),#上装
        3504: (9, 225),#针织衫
        3506: (9, 223),#外套
        1205: (9, 226),#裤装
        3276: (9, 230),#套装
        3509: (9, 228),#卫衣
        3508: (9, 224),#打底装
        3277: (9, 220),#T恤
        3278: (9, 221),#衬衫
        3510: (9, 223),#冬装
        3507: (9, 226),#休闲裤
        3505: (9, 226),#牛仔裤

        #内衣
        1207: (9, 224),#文胸
        3252: (9, 224),#内裤
        1029: (9, 224),#运动打底
        1028: (9, 231),#居家装
        3254: (1, 109),#女士袜子
        1031: (9, 224),#修型塑身
        3253: (1, 109),#男士袜子
        3253: (11, 253),#男士内裤

        #箱包
        1056: (8, 0),#女包
        3296: (8, 212),#手提包
        3525: (8, 211),#单肩包
        3524: (8, 210),#女士钱包
        3530: (8, 211),#双肩包
        1058: (8, 215),#功能箱包
        3531: (8, 211),#胸包腰包
        1057: (8, 0),#男包
        3529: (8, 211),#斜挎包
        3526: (8, 211),#商务包
        3527: (8, 216),#男士钱包

        #居家生活
        3584: (0, 0),#保暖防护
        1039: (2, 128),#百货日杂
        1041: (2, 128),#个护洗护
        3284: (2, 128),#卫生用品
        1041: (2, 124),#厨具烹饪
        3583: (2, 122),#整理收纳
        3110: (2, 128),#中老年用品
        3284: (2, 127),#宠物用品

        #家纺家装
        3591: (2, 120),#被子
        3590: (2, 120),#订单被罩
        3592: (2, 129),#毯子抱枕
        1036: (2, 120),#床上用品
        3594: (2, 129),#地垫
        1037: (2, 123),#家居饰品
        3593: (2, 120),#枕头
        3595: (2, 123),#桌面窗帘
        1038: (2, 123),#家具建材
        3283: (2, 123),#五金灯饰

        #家电
        3582: (2, 128),#秋冬保暖
        3302: (4, 152),#手机周边
        3297: (2, 121),#生活家电
        3298: (2, 121),#厨房电器 
        3301: (4, 154),#电脑配件
        3300: (4, 154),#影音相机
        3299: (4, 128),#护理按摩

        #美妆
        1042: (0, 0),#美容护肤
        1043: (0, 0),#彩妆香氛
        1044: (0, 0),#个人洗护
        1045: (0, 0),#美甲美发
        3585: (1, 105),#百搭围巾
        3589: (1, 103),#发饰
        3587: (1, 104),#帽子
        3586: (1, 110),#腰带
        3304: (1, 0),#首饰
        3306: (1, 0),#服装配饰
        3307: (1, 0),#其他饰品
        3305: (1, 107),#手表

        #儿童
        1059: (13, 282),#儿童衣橱
        1060: (13, 282),#儿童鞋品
        1061: (13, 283),#儿童玩具
        3517: (13, 282),#儿童上衣
        3518: (13, 282),#裤装
        3519: (13, 282),#内衣
        3295: (13, 282),#套装
        3520: (13, 282),#运动鞋
        3521: (13, 282),#皮鞋
        3522: (13, 282),#帆布鞋子
        3523: (13, 284),#益智玩具

        #母婴
        3523: (13, 284),#益智玩具
        "yingyouchuanzhuo": (13, 282),#母婴穿着
        "wanjuzaojiao": (13, 283),#玩具早教
        "zhiniaoku": (13, 280),#纸尿裤
        "yingyouyongpin": (13, 280),#婴幼用品
        "yunmabibei": (13, 281),#孕妇必备

        #男装
        1032: (11, 0),#上衣
        1033: (11, 255),#裤装
        3581: (11, 252),#外套
        1034: (11, 0),#套装
        3577: (11, 256),#卫衣
        3578: (11, 250),#衬衫
        3579: (11, 257),#针织衫
        2382: (11, 251),#T恤
        3574: (11, 252),#夹克
        3576: (11, 252),#羽绒服
        3281: (11, 255),#休闲裤
        3580: (11, 255),#牛仔裤

        #文娱活动：
        3310: (14, 291),#运动装备
        3311: (14, 291),#运动鞋服
        3313: (14, 290),#户外装备
        3312: (14, 290),#户外鞋服
        1074: (3, 0),#办公文具
        1075: (14, 291),#车饰车品
}

WISHING_CATE_ITEM = None

def random_datetime():
    now = datetime.datetime.now()
    hour = now.hour + 2 if (now.hour + 2) < 22 else now.hour
    new_dt = datetime.datetime(year = now.year, month = now.month, day = now.day, hour = hour, minute = random.randint(1,59), second = random.randint(1, 59))
    return new_dt

def fetch_url(url, method = 'GET', data = {}, headers = {}, follow_redirects = True, use_proxy=False):
    if data:
        body = urllib.urlencode(data)
    else:
        body = None

    try:
        resp = requests.get(url, data=body, headers=headers)
        resp.raise_for_status()

        global WISHING_CATE_ITEM
        result_json = json.loads(resp.text)
        for goods in result_json['objects']:
            save_item(goods, WISHING_CATE_ITEM)
        has_next = result_json['meta']['has_next']
        return has_next

    except requests.RequestException as e:
        print "Error request : ", url

    return False 

def do_crawl(url):
    return fetch_url(url, headers = {
            'User-Agent': IOS_UA, 
            'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'X-Zhen800filter':"110001110000000000000000000000000000000000000000000000000000000000000000000000000000000000",
            'X-Zhen800out':"0,21,0,0,0,0,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0",
            'Cookie': "__cartlist_status=0; __page_status=%7B%22cartnum%22%3A%220%22%7D; _ga=GA1.2.1438167779.1443971607; channelId=b2e1cc; cId=wireless3592; cType=0; dealId=1680199; deviceId=9adc0955a916c83a2707a50221cfdccbd5407eb9; platform=iPhone; session_id=1581846041.1443847261; source=tao800_app; user_id=1438167779.1443971607; userId=; utm_csr_first=direct; version=4.0.2; X-Zhe800filter=100011100000000000000000000000000000000000000000000000000000000000000000000000000000000000; X-Zhe800out=undefined",
        }, use_proxy = False)

def save_item(goods, cate_item):
    origin_url = goods.get('origin_deal_url')
    if not origin_url:
        return
    goods_obj = models.Goods()
    title = goods['short_title']
    if not title:
        title = goods['title']

    goods_obj.title = title

    goods_obj.cate_pid = cate_item[0]
    goods_obj.cate_id = cate_item[1]

    goods_obj.price_now = Decimal(float(goods['price'])/100)
    goods_obj.rage_begin = datetime.datetime.now()
    expire_time = datetime.datetime.now() + datetime.timedelta(days = 3)
    goods_obj.rage_end = expire_time
    goods_obj.source = CURRENT_SOURCE
    goods_obj.is_onsale = False
    goods_obj.date_created = random_datetime()
    def _get_et(res_data):
        ps = PATTERN.search(res_data)
        if not ps:
            return
        ts = ps.groups()
        if not ts:
            return

        t_id = ts[0]
        pic_url = goods['image_url']['hd3']
        goods_exit = None
        try:
            goods_exit = models.Goods.objects.get(t_id = t_id)
        except models.Goods.DoesNotExist, e:
            pass

        if not goods_exit:
            pic_url = qiniu_util.upload(CURRENT_SOURCE, pic_url)
            goods_obj.pic_url = pic_url
            goods_obj.t_id = t_id
            print 'add-----------', str(t_id)
            goods_obj.save()
        else:
            print 'update-----------', str(t_id)

    _get_et(origin_url)

def main():
    global WISHING_CATE_ITEM
    main_url = "http://m.api.zhe800.com/v5/deals?page=$page&per_page=20&user_role=4&user_type=1&url_name=$name"
    for k, v in CATE_MAP.items():
        WISHING_CATE_ITEM = v
        cate_url = main_url.replace('$name', "wireless"+str(k))
        for page in range(1, 50):
            target_url = cate_url.replace('$page', str(page))
            has_next = do_crawl(target_url)
            if not has_next:
                break;

if __name__ == '__main__':
    main()
