#/usr/bin/python
#coding:utf-8

from wishing import models;
from contrib import assign_util, qiniu_util
import time, datetime
import random, re
import json
import requests
import urllib
from decimal import Decimal

CURRENT_SOURCE = 2

IOS_UA = 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_1_1 like Mac OS X) AppleWebKit/537.51.2 (KHTML, like Gecko) Mobile/11D201'

CATE_MAP = {
    "2":  (16, 0),#创意
    "1":  (2, 0),#家居
    "6":  (5, 0),#食品
    "5":  (0, 0),#美妆
    "12": (0, 0),#杂货
    "15": (0, 0),#穿搭
    "16": (0, 0),#鞋包
    "17": (6, 182),#男士
    "10": (2, 0),#厨具
    "14": (4, 0),#数码
    "3":  (3, 0),#办公
    "8":  (0, 0),#主题
    "18": (0, 0),#书籍
    "13": (0, 0),#运动
    "4":  (0, 0),#卫浴
    "9":  (16, 311),#植物
}

DATA = {
    "app_installtime": "1442129373.487766",
    "app_versions": "4.2.1",
    "channel_name": "appStore",
    "client_id": "bt_app_ios",
    "client_secret": "9c1e6634ce1c5098e056628cd66a17a5",
    "os_versions": "9.0.2",
    "pagesize": "20",
    "screensize": "750",
    "track_device_info": "iPhone7,2",
    "track_deviceid": "940323C4-3E6F-40C9-8810-BA1539B39DD1",
    "v": "7",
    #"category": "0",
    "page": "0",
}

def random_datetime():
    now = datetime.datetime.now()
    hour = now.hour + 2 if (now.hour + 2) < 22 else now.hour
    new_dt = datetime.datetime(year = now.year, month = now.month, day = now.day, hour = hour, minute = random.randint(1,59), second = random.randint(1, 59))
    return new_dt

def save_item(goods, cate_item):
    goods_obj = models.Goods()
    price = goods['price']
    if not price:
        return;
    if " - " in price:
        price = price.split(" - ")[0]
    goods_obj.price_now = Decimal(price)
    
    goods_obj.title = goods['title'].encode('utf-8')
    goods_obj.cate_pid = cate_item[0]
    goods_obj.cate_id = cate_item[1]

    goods_obj.rage_begin = datetime.datetime.now()
    expire_time = datetime.datetime.now() + datetime.timedelta(days = 3)
    goods_obj.rage_end = expire_time
    goods_obj.source = CURRENT_SOURCE
    goods_obj.is_onsale = False
    goods_obj.date_created = random_datetime()
    def _get_et(res_data):
        t_id = goods['item_id']
        pic_url = goods['pic']
        goods_exit = None
        try:
            goods_exit = models.Goods.objects.get(t_id = t_id)
        except models.Goods.DoesNotExist, e:
            pass
        if not goods_exit:
            pic_url = qiniu_util.upload(CURRENT_SOURCE, pic_url)
            goods_obj.pic_url = pic_url

            goods_obj.t_id = t_id
            print 'add-----------', str(t_id)
            goods_obj.save()
        else:
            print 'update-----------', str(t_id)

    if goods['item_id']:
         _get_et(goods)
         
#当还有下页时返回True, 没有下页或报错时返回False
def fetch_url(url, method = 'POST', data = {}, headers = {}, follow_redirects = True, use_proxy=False):
    try:
        resp = requests.post(url, data=data, headers=headers)
        resp.raise_for_status()
        global CATE_MAP
        result_json = json.loads(resp.text)
        if result_json['status'] == 1:
            goods_list = result_json['data']['product']
            for goods in goods_list:
                category = goods['category']
                cate_item = CATE_MAP.get(category)
                if not cate_item:
                    cate_item = (0, 0)

                #cate_item = assign_util.get_cate_t(cate_item, goods['title'])

                save_item(goods, cate_item)
            if len(goods_list) < 20:
                #没有下一页了
                return False
            else:
                return True
        else:
            print "Error result_code:", str(result_json['status'])
    except requests.RequestException as e:
        print "Error requests: ", url

    return False

def do_crawl(url, data):
    return fetch_url(url, data = data, headers = {'User-Agent': IOS_UA, 'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'}, use_proxy = False)

def main():
    target_url = "http://open3.bantangapp.com/product/list"
    global DATA
    page = 0
    has_next = True
    #当有下页时一直进行
    while has_next:
        DATA['page'] = str(page)
        has_next = do_crawl(target_url, DATA)
        page = page + 1

if __name__ == '__main__':
    main()


