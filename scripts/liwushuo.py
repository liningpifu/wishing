#/usr/bin/python
#coding:utf-8

from wishing import models
from contrib import qiniu_util
import time, datetime
import random, re
import json
import requests
import urllib
from decimal import Decimal

CURRENT_SOURCE = 1

IOS_UA = 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_1_1 like Mac OS X) AppleWebKit/537.51.2 (KHTML, like Gecko) Mobile/11D201'

CATE_MAP = {
        7: (0, 0),#智能设备
        6: (0, 0),#鲜花蛋糕
        5: (4, 144),#书
        4: (16, 310),#模型拼图
        3: (16, 313),#毛绒公仔
        2: (16, 312),#动漫明星
        1: (0, 0),#创意礼品
        #配饰
        13: (1, 100),#手饰
        12: (1, 107),#手表
        11: (1, 104),#帽子
        10: (1, 106),#镜框
        9:  (1, 103),#发饰
        8:  (1, 102),#耳饰
        14: (1, 108),#手套
        15: (1, 105),#围巾/口罩
        16: (1, 101),#项链
        91: (1, 110),#腰带
        96: (1, 109),#袜子
        #家居
        18: (2, 129),#抱枕/靠垫/坐垫
        29: (16, 311),#装饰画
        28: (16, 311),#钟表摆件
        27: (16, 311),#香薰用品
        26: (2, 122),#收纳整理
        20: (2, 128),#生活神器
        24: (2, 121),#生活家电
        23: (2, 128),#雨伞
        22: (2, 120),#家居纺饰
        19: (2, 127),#宠物用品
        95: (2, 130),#手帕/毛巾
        30: (2, 126),#保温杯/便当盒
        31: (2, 126),#杯子
        32: (2, 125),#餐具
        33: (2, 125),#吃货神器
        34: (2, 124),#厨具
        35: (2, 124),#烘焙道具
        38: (2, 126),#酒具/茶具
        #办公
        47: (3, 140),#办公收纳
        48: (3, 141),#本子
        49: (3, 141),#笔
        50: (3, 141),#贺卡/明信片
        51: (3, 141),#卡包/卡套/钥匙包
        52: (3, 143),#手账相关
        53: (3, 142),#台历/日历
        54: (3, 142),#相框/相册
        141: (3, 141),#书签
        #数码
        39: (4, 154),#电脑周边
        40: (4, 155),#耳机/音响
        41: (4, 150),#摄影周边
        42: (4, 153),#手机壳
        43: (4, 152),#手机周边
        46: (4, 151),#移动电源
        #美食
        81: (5, 161),#茶
        97: (5, 166),#速食/即食
        92: (5, 165),#健康代餐
        89: (5, 160),#糖果糕点
        88: (5, 167),#曲奇饼干
        87: (5, 164),#巧克力
        86: (5, 163),#膨化食品
        85: (5, 161),#咖啡/冲饮
        83: (5, 162),#坚果
        82: (5, 162),#果干/果酱
        189: (5, 168),#饮料/雪糕
        #护理
        143: (6, 180),#女士香水
        154: (6, 175),#唇部护理
        155: (6, 174),#手部护理
        157: (6, 178),#身体护理
        159: (6, 173),#头发护理
        160: (6, 179),#护肤套装
        162: (6, 182),#男士护肤
        163: (6, 182),#剃须刀
        181: (6, 177),#手工皂
        153: (6, 177),#眼部护理
        152: (6, 176),#防晒/晒后修复
        144: (6, 181),#男士香水
        145: (6, 170),#卸妆
        146: (6, 170),#洁面
        147: (6, 171),#爽肤水
        148: (6, 171),#乳液/面霜
        149: (6, 177),#精华
        150: (6, 177),#喷雾
        151: (6, 172),#面膜
        182: (6, 177),#鼻部清洁
        #美妆
        166: (7, 194),#底妆
        167: (7, 193),#腮红
        169: (7, 197),#高光
        170: (7, 190),#眼妆
        171: (7, 191),#唇彩/口红
        172: (7, 192),#美甲
        173: (7, 195),#美妆工具
        174: (7, 196),#彩妆套装
        187: (7, 192),#假发
        #箱包
        55: (8, 211),#单肩包
        57: (8, 214),#化妆包
        58: (8, 210),#女士钱包
        59: (8, 213),#手拿包
        60: (8, 212),#手提包
        61: (8, 211),#双肩包
        62: (8, 215),#行李箱
        142: (8, 216),#男士钱包
        188: (8, 212),#帆布包/帆布袋
        #女装：
        99: (9, 220),#短T
        176: (9, 223),#冬季外套
        175: (9, 220),#无袖背心
        168: (9, 229),#情侣装
        116: (14, 291),#泳衣
        114: (9, 224),#内衣
        113: (9, 231),#家居服
        111: (9, 226),#长裤
        110: (9, 227),#短裤
        109: (9, 230),#套装
        108: (9, 222),#半身裙
        107: (9, 222),#连衣裙
        106: (9, 230),#运动套装
        105: (9, 221),#春夏/外套/开衫
        104: (9, 228),#卫衣
        102: (9, 220),#长T
        101: (9, 221),#衬衫
        100: (9, 221),#雪纺衫
        178: (9, 225),#毛衣
        #女鞋
        117: (10, 240),#平底鞋
        118: (10, 243),#高跟鞋
        122: (10, 241),#凉鞋
        123: (10, 244),#休闲鞋
        124: (10, 246),#运动鞋
        125: (10, 245),#家居鞋
        183: (10, 244),#帆布鞋
        185: (10, 245),#雨鞋
        186: (10, 242),#短靴
        #男装
        129: (11, 250),#衬衫
        130: (11, 251),#短T
        131: (11, 251),#长T
        132: (11, 254),#短裤
        133: (11, 255),#长裤
        135: (11, 252),#外套
        177: (11, 256),#卫衣
        179: (11, 253),#内衣
        180: (11, 257),#毛衣
        #男鞋
        136: (12, 270),#皮鞋
        138: (12, 271),#休闲鞋
        139: (12, 272),#运动鞋
        184: (12, 271),#帆布鞋
        #母婴
        74: (13, 282),#童装
        75: (13, 280),#宝宝用品
        76: (13, 283),#玩具
        77: (13, 281),#孕妇用品
        78: (13, 283),#早教
        #户外
        79: (14, 290),#户外装备
        80: (14, 291),#运动健身
        #保健品
        63: (15, 300),#保健品
        66: (15, 301),#口腔
        72: (15, 302),#眼部
        93: (15, 303),#女性
}

WISHING_CATE_ITEM = None

def random_datetime():
    now = datetime.datetime.now()
    hour = now.hour + 2 if (now.hour + 2) < 22 else now.hour
    new_dt = datetime.datetime(year = now.year, month = now.month, day = now.day, hour = hour, minute = random.randint(1,59), second = random.randint(1, 59))
    return new_dt

def fetch_url(url, method = 'GET', data = {}, headers = {}, follow_redirects = True, use_proxy=False):
    if data:
        body = urllib.urlencode(data)
    else:
        body = None

    try:
        resp = requests.get(url, data=body, headers=headers)
        resp.raise_for_status()
        
        global WISHING_CATE_ITEM
        result_json = json.loads(resp.text)
        if result_json['code'] == 200:
            for goods in result_json['data']['items']:
                save_item(goods, WISHING_CATE_ITEM)
            next_url = result_json['data']['paging']['next_url']
            if next_url:
                return next_url
            return None
        else:
            print "Error result_code:", str(result_json['code'])
    except requests.RequestException as e:
        print "Error fetch:", url

    return None

def do_crawl(url):
    return fetch_url(url, headers = {'User-Agent': IOS_UA, 'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'}, use_proxy = False)

def save_item(goods, cate_item):
    goods_obj = models.Goods()
    goods_obj.title = goods['name'].encode('utf-8')
    goods_obj.cate_pid = cate_item[0]
    goods_obj.cate_id = cate_item[1]
    pic_url = goods['cover_image_url']
    #if "liwushuo" in pic_url:
    #    pic_url = qiniu_util.upload(CURRENT_SOURCE, pic_url)
    goods_obj.pic_url = pic_url

    goods_obj.price_now = Decimal(goods['price'])
    goods_obj.rage_begin = datetime.datetime.now()
    expire_time = datetime.datetime.now() + datetime.timedelta(days = 3)
    goods_obj.rage_end = expire_time
    goods_obj.source = CURRENT_SOURCE
    goods_obj.is_onsale = False
    goods_obj.date_created = random_datetime()
    def _get_et(res_data):
        t_id = goods['purchase_id']
        pic_url = goods['cover_image_url']
        goods_exit = None
        try:
            goods_exit = models.Goods.objects.get(t_id = t_id)
        except models.Goods.DoesNotExist, e:
            pass
        if not goods_exit:
            #保存一份图片
            if "liwushuo" in pic_url:
                pic_url = qiniu_util.upload(CURRENT_SOURCE, pic_url)
            goods_obj.pic_url = pic_url

            goods_obj.t_id = t_id
            print 'add-----------', str(t_id)
            goods_obj.save()
        else:
            print 'update-----------', str(t_id)

    if goods['purchase_id']:
         _get_et(goods)

def main():
    global WISHING_CATE_ITEM
    main_url = "http://api.liwushuo.com/v2/item_subcategories/$cid/items?limit=20&offset=0"
    #main_url = "http://api.liwushuo.com/v2/items?gender=1&generation=0&limit=20&offset=0"
    for k, v in CATE_MAP.items():
        WISHING_CATE_ITEM = v
        target_url = main_url.replace("$cid", str(k))
        #如果一直有下一页
        while target_url:
            target_url = do_crawl(target_url)

if __name__ == '__main__':
    main()


