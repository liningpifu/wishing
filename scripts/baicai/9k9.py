#/usr/bin/python
#coding:utf-8

from wishing import models;
import time, datetime
import random, re
import json
import requests
import urllib
from decimal import Decimal

CURRENT_SOURCE = 3

IOS_UA = 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_1_1 like Mac OS X) AppleWebKit/537.51.2 (KHTML, like Gecko) Mobile/11D201'

CATE_MAP = {
        "cate_1": "1",
        "cate_2": "2",
        "cate_3": "3",
        "cate_4": "4",
        "cate_5": "5",
        "cate_6": "6",
        "cate_7": "7",
        "cate_8": "8",
        "cate_9": "9",
}

MAP_CATE = {v: k for k,v in CATE_MAP.items()}

def random_datetime():
    now = datetime.datetime.now()
    hour = now.hour + 2 if (now.hour + 2) < 22 else now.hour
    new_dt = datetime.datetime(year = now.year, month = now.month, day = now.day, hour = hour, minute = random.randint(1,59), second = random.randint(1, 59))
    return new_dt

def fetch_url(url, method = 'GET', data = {}, headers = {}, follow_redirects = True, use_proxy=False):
    print 'fetch_url', url
    if data:
        body = urllib.urlencode(data)
    else:
        body = None

    try:
        resp = requests.get(url, data=body, headers=headers)
        resp.raise_for_status()
    except requests.RequestException as e:
        print "Error :", url
    else:
        parse_resp(resp)

def do_crawl(url):
    fetch_url(url, headers = {'User-Agent': IOS_UA, 'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'}, use_proxy = False)

def parse_resp(resp):
    global MAP_CATE
    result_json = json.loads(resp.text)
    url_name = re.search(r'cid=(\w+)', resp.request.url).group(1)
    url_name = MAP_CATE.get(url_name)
    for good in result_json['list']:
        save_item(good, url_name)

def save_item(goods, url_name):
    goods_obj = models.Goods()
    goods_obj.title = goods['title'].encode('utf-8')
    goods_obj.cate_id = models.CATE_D.get(url_name)
    goods_obj.pic_url = goods['pic_url']
    goods_obj.price_now = Decimal(goods['now_price'])
    goods_obj.price_ori = Decimal(goods['origin_price'])
    goods_obj.rage_begin = datetime.datetime.strptime(goods['start_discount'], '%Y-%m-%d %H:%M:%S')
    expire_time = datetime.datetime.now() + datetime.timedelta(days = 3)
    goods_obj.rage_end = expire_time
    goods_obj.source = CURRENT_SOURCE
    goods_obj.is_onsale = False
    goods_obj.date_created = random_datetime()
    def _get_et(res_data):
        t_id = goods['num_iid']
        goods_exit = None
        try:
            goods_exit = models.Goods.objects.get(t_id = t_id)
        except models.Goods.DoesNotExist, e:
            pass
        if not goods_exit:
	    goods_obj.t_id = t_id
            print 'add-----------', str(t_id)
            goods_obj.save()
        else:
            print 'update-----------', str(t_id)

    if goods['num_iid']:
         _get_et(goods)

def main():
    print str(datetime.datetime.now())
    main_url = "http://jkjby.yijia.com/jkjby/view/list_api.php?cid=$cid"
    for k, v in CATE_MAP.items():
        target_url = main_url.replace('$cid', v)
        do_crawl(target_url)

if __name__ == '__main__':
    main()


