from django.shortcuts import render
from django.core.serializers.json import DjangoJSONEncoder
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from auction.models import Author,Art,Order

from django.views.decorators.csrf import csrf_exempt
import json

# Create your views here.

def index(request):
    params = {}
    return render_to_response('auction/index.html', params)

def list(request, authorid):
    author_id = int(authorid)
    author = Author.objects.get(id=author_id)
    author.view_count = author.view_count+1
    author.save()
    list_qs = Art.objects.filter(author_id=author_id)
    art_list = []

    i = 0
    item_list = []
    for art in list_qs:
      if i == 0:
          item_list = []
          item_list.append(art)
          i = 1
      else:
          item_list.append(art)
          art_list.append(item_list)
          i = 0

    print i
    print item_list
    if i==1 and item_list:
        if len(item_list) == 1:
            item_list.append(None)

        art_list.append(item_list)

    params = {"list": art_list, "author": author}
    return render_to_response('auction/list.html', params)

def detail(request, artid):
    art_id = artid

    art_obj = Art.objects.get(art_id=art_id)
    art_obj.view_count = art_obj.view_count + 1
    art_obj.save()

    params = {"art": art_obj}
    return render_to_response('auction/detail.html', params)

@csrf_exempt
def pre_order(request):
    callback = request.GET.get('callback')

    art_id = request.POST.get("art_id")
    name = request.POST.get("name")
    mobile = request.POST.get("mobile")
    weixin = request.POST.get("weixin")

    order_obj = Order()
    order_obj.art_id = art_id
    order_obj.name = name
    order_obj.mobile = mobile
    order_obj.weixin = weixin
    order_obj.save()

    params = {"status": True}
    return HttpResponse(callback + '(' + json.dumps(params) + ')', content_type = 'application/javascript')

