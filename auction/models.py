from django.db import models

# Create your models here.

class Author(models.Model):
    name = models.CharField(max_length = 64, default="")
    mobile = models.CharField(max_length = 11, default="")
    cover = models.CharField(max_length = 256, default = "")
    desc = models.CharField(max_length = 512, default="")
    view_count = models.IntegerField(default=0)

class Art(models.Model):
    author_id = models.IntegerField(default=0)
    art_id = models.CharField(max_length=32, default="")
    name = models.CharField(max_length = 64, default="")
    cover = models.CharField(max_length = 256, default = "")
    desc = models.CharField(max_length = 512, default="")
    price_auction = models.IntegerField(default=0)
    price_market = models.CharField(max_length=64, default="")
    measurement = models.CharField(max_length=64, default="")
    date_created = models.DateTimeField(null = True, blank = True)
    date_updated = models.DateTimeField(null = True, blank = True)
    view_count = models.IntegerField(default=0)

class Order(models.Model):
    art_id = models.CharField(max_length=32, default="")
    name = models.CharField(max_length=32,default="")
    mobile = models.CharField(max_length=16,default="")
    weixin = models.CharField(max_length=32,default="")
    status = models.IntegerField(default=0)
    remark = models.CharField(max_length=128, default="")
    date_created = models.DateTimeField(auto_now_add=True, blank = True)
    date_updated = models.DateTimeField(auto_now=True, blank = True)
