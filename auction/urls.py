#coding:utf-8
from django.conf.urls import patterns, url

urlpatterns = patterns('',
    (r'^index/?$', 'auction.views.index'),
    (r'^list/(?P<authorid>\d+)?$', 'auction.views.list'),
    (r'^detail/(?P<artid>\d+)?$', 'auction.views.detail'),
    (r'^pre_order/?$', 'auction.views.pre_order'),
)
