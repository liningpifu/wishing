export APPROOT=$PWD
export LIBROOT=$APPROOT/../libs
export PYTHONPATH=$PWD:$LIBROOT:$PYTHONPATH

function set_settings() {
    mood=development

    if [ -f .site_mood ]; then
        mood=`cat .site_mood`
    fi
    export TORNADO_LOGGING_CONF="logging_$mood.conf"
    export HANDLE_LOGGING_CONF="logging_handle.conf"
    export RUN_MOOD=$mood
	export DJANGO_SETTINGS_MODULE="main.settings"
}

set_settings
mkdir -p /tmp/$USER/

PYTHON=python
alias run_script="$PYTHON"
alias ygrep='grep --exclude-dir=static --exclude-dir=app/fixtures -irne'
alias pygrep='find . -name "*.py" | xargs grep -nE'
