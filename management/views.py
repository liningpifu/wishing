#coding:utf-8
from django.shortcuts import render
from django.core.serializers.json import DjangoJSONEncoder
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template.context import RequestContext

from wishing import models as wishing_models
from django.core.paginator import Paginator

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import User, AbstractUser

from django.views.decorators.csrf import csrf_exempt
import json

def admin_check(user):
    return user.is_superuser

@csrf_exempt
def signin(request):
    if request.method == 'POST':
        u_name = request.POST.get('username')
        u_pass = request.POST.get('password')
        user = authenticate(username = u_name, password = u_pass)
        if user is not None:
            login(request, user)
            return HttpResponseRedirect("/management/index/")
        else:
            rtn_data = {"status": False, "message": "用户名或密码错误"}
            return render_to_response('manage/signin.html', rtn_data)
    else:
        print '*****************>',request.GET.get('next')
        return render_to_response('manage/signin.html')

@user_passes_test(admin_check, login_url = "/management/signin/")
def index(request):
    count_onsale = wishing_models.Goods.objects.filter(is_onsale = 1, is_deleted = 0).count()
    count_trans = wishing_models.Goods.objects.filter(cate_id__gt = 0, is_onsale = 0, is_deleted = 0).count()
    count_assign = wishing_models.Goods.objects.filter(cate_id = 0, is_deleted = 0).count()
    count_deleted = wishing_models.Goods.objects.filter(is_deleted = 1).count()
    params = {
            "count_onsale": count_onsale,
            "count_trans": count_trans,
            "count_assign": count_assign,
            "count_deleted": count_deleted,
    }
    return render_to_response('manage/index.html', params)

@csrf_exempt
@user_passes_test(admin_check, login_url = "/management/signin/")
def goods_list(request, p_cid):
    p_cid = int(p_cid)
    cid = int(request.GET.get('cid', "0"))
    cate_list = wishing_models.CATE_MAP.get(p_cid)

    if cid in cate_list:
        goods_list_qs = wishing_models.Goods.objects.filter(cate_id = cid, is_onsale = 1, is_deleted = 0).order_by('-date_onsale')
    else:
        goods_list_qs = wishing_models.Goods.objects.filter(cate_id__in = cate_list, is_onsale = 1, is_deleted = 0).order_by('-date_onsale')

    if not request.GET.has_key('aoData'):
        if cid in cate_list:
            cate_sel = wishing_models.CATE_NAME_DICT.get(cid)
        else:
            cate_sel = "全部"

        cate_items = wishing_models.CATE_T_MAP.get(p_cid)

        params = {
                "count": goods_list_qs.count(),
                "cate_sel": cate_sel,
                "cate_items": cate_items,
                }

        return render_to_response('manage/goods_list.html', params, context_instance=RequestContext(request))
    else:
        sEcho = 0;
        iDisplayStart = 0;
        iDisplayLength = 0;

        aoData = request.GET.get('aoData')
        data_map = json.loads(aoData)
        for dm in data_map:
            if dm.get('name') == "sEcho":
                sEcho = int(dm.get('value'))
            elif dm.get('name') == "iDisplayStart":
                iDisplayStart = int(dm.get('value'))
            elif dm.get('name') == "iDisplayLength":
                iDisplayLength = int(dm.get('value'))

        count = goods_list_qs.count()
        goods_list = goods_list_qs[iDisplayStart: iDisplayStart+iDisplayLength]

        params = {
            "Echo":sEcho,
            "iTotalRecords":count,
            "iTotalDisplayRecords":count,
            "aaData":[g.as_dict() for g in goods_list]
        }
        return HttpResponse(json.dumps(params, cls=DjangoJSONEncoder), content_type = 'application/json')

@csrf_exempt
@user_passes_test(admin_check, login_url = "/management/signin/")
def trans_list(request, p_cid):
    p_cid = int(p_cid)
    cid = int(request.GET.get('cid', "0"))
    cate_list = wishing_models.CATE_MAP.get(p_cid)

    if cid in cate_list:
        goods_list_qs = wishing_models.Goods.objects.filter(cate_id = cid, is_onsale = 0, is_deleted = 0).order_by('-date_created')
    else:
        goods_list_qs = wishing_models.Goods.objects.filter(cate_id__in = cate_list, is_onsale = 0, is_deleted = 0).order_by("-date_created")

    if not request.GET.has_key('aoData'):
        if cid in cate_list:
            cate_sel = wishing_models.CATE_NAME_DICT.get(cid)
        else:
            cate_sel = "全部"

        cate_items = wishing_models.CATE_T_MAP.get(p_cid)

        params = {
                "count": goods_list_qs.count(),
                "cate_sel": cate_sel,
                "cate_items": cate_items,
                }

        return render_to_response('manage/goods_list.html', params, context_instance=RequestContext(request))
    else:
        sEcho = 0;
        iDisplayStart = 0;
        iDisplayLength = 0;

        aoData = request.GET.get('aoData')
        data_map = json.loads(aoData)
        for dm in data_map:
            if dm.get('name') == "sEcho":
                sEcho = int(dm.get('value'))
            elif dm.get('name') == "iDisplayStart":
                iDisplayStart = int(dm.get('value'))
            elif dm.get('name') == "iDisplayLength":
                iDisplayLength = int(dm.get('value'))

        count = goods_list_qs.count()
        goods_list = goods_list_qs[iDisplayStart: iDisplayStart+iDisplayLength]

        params = {
            "Echo":sEcho,
            "iTotalRecords":count,
            "iTotalDisplayRecords":count,
            "aaData":[g.as_dict() for g in goods_list]
        }
        return HttpResponse(json.dumps(params, cls=DjangoJSONEncoder), content_type = 'application/json')

@csrf_exempt
@user_passes_test(admin_check, login_url = "/management/signin/")
def assign_list(request):
    goods_list_qs = wishing_models.Goods.objects.filter(cate_id = 0, is_deleted = 0).order_by("-date_created")

    pcate_items = wishing_models.P_CATE_CHOICES

    params = {
            "count": goods_list_qs.count(),
            "goods_list":goods_list_qs[0:10],
            "pcate_items":pcate_items,
            }

    return render_to_response('manage/assign_list.html', params, context_instance=RequestContext(request))

@csrf_exempt
@user_passes_test(admin_check, login_url = "/management/signin/")
def goods_set_cate(request):
    callback = request.GET.get('callback')
    goods_id = int(request.POST.get('goods_id'))
    cate_id = int(request.POST.get('cate_id'))
    cate_pid = int(request.POST.get('cate_pid'))
    try:
        goods_obj = wishing_models.Goods.objects.get(id = goods_id)
        goods_obj.cate_id = cate_id
        goods_obj.cate_pid = cate_pid
        goods_obj.save()
        params = {"status":True, "cate_sel": wishing_models.CATE_NAME_DICT.get(cate_id)}
    except wishing_models.Goods.DoesNotExist, e:
        params = {"status":False}

    return HttpResponse(callback + '(' + json.dumps(params) + ')', content_type = 'application/javascript')

@csrf_exempt
@user_passes_test(admin_check, login_url = "/management/signin/")
def goods_set_deleted(request):
    callback = request.GET.get('callback')
    goods_id = int(request.POST.get('goods_id'))
    try:
        goods_obj = wishing_models.Goods.objects.get(id = goods_id)
        goods_obj.is_deleted = 1
        goods_obj.is_onsale = 0
        goods_obj.save()
        params = {"status":True, "result":"已删除"}
    except wishing_models.Goods.DoesNotExist, e:
        params = {"status":False}

    return HttpResponse(callback + '(' + json.dumps(params) + ')', content_type = 'application/javascript')

@csrf_exempt
@user_passes_test(admin_check, login_url = "/management/signin/")
def goods_edit(request):
    if request.method == "GET":
        gid = int(request.GET.get('gid'))
        try:
            goods_obj = wishing_models.Goods.objects.get(id = gid)
        except wishing_models.Goods.DoesNotExist, e:
            return HttpResponse("商品不存在")

        if goods_obj.cate_pid != 0:
            cate_items = wishing_models.CATE_T_MAP.get(goods_obj.cate_pid)
        else:
            cate_items = None

        params = {
                "goods_obj": goods_obj,
                "pcate_items": wishing_models.P_CATE_CHOICES,
                "cate_items": cate_items,
            }
        return render_to_response('manage/goods_edit.html', params, context_instance=RequestContext(request))
    else:
        gid = int(request.POST.get('gid'))
        title = request.POST.get('title')
        cate_pid = int(request.POST.get('pcate', '0'))
        cate_id = int(request.POST.get('cate', '0'))
        detail = request.POST.get('detail')
        detail_pic = request.POST.get('detail_pic')

        if not title:
            return HttpResponse("商品名称不能为空")
        if cate_pid <= 0 or cate_id <= 0:
            return HttpResponse("商品类别未指定")

        try:
            goods_obj = wishing_models.Goods.objects.get(id = gid)
            goods_obj.title = title
            goods_obj.cate_id = cate_id
            goods_obj.cate_pid = cate_pid
            goods_obj.detail = detail
            goods_obj.detail_pic = detail_pic
            goods_obj.save()
        except wishing_models.Goods.DoesNotExist, e:
            return HttpResponse("商品不存在")

        cate_items = wishing_models.CATE_T_MAP.get(goods_obj.cate_pid)
        params = {
                "goods_obj": goods_obj,
                "pcate_items": wishing_models.P_CATE_CHOICES,
                "cate_items":cate_items,
                "message":"修改成功",
            }

        return render_to_response('manage/goods_edit.html', params, context_instance=RequestContext(request))

@csrf_exempt
@user_passes_test(admin_check, login_url = "/management/signin/")
def cate_list(request):
    callback = request.GET.get('callback')
    cate_pid = int(request.POST.get('pcate'))
    cate_t_list = wishing_models.CATE_T_MAP.get(cate_pid)
    cate_list = []
    for cate_t in cate_t_list:
        cate_map = {"id": cate_t[0], "name": cate_t[1]}
        cate_list.append(cate_map)
    params = {"cate_list": cate_list}
    print params
    return HttpResponse(callback + '(' + json.dumps(params) + ')', content_type = 'application/javascript')

