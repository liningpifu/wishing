#coding:utf-8
from django.conf.urls import patterns, url

urlpatterns = patterns('',
    (r'^signin/?$', 'management.views.signin'),  
    (r'^index/?$', 'management.views.index'),
    (r'^list/?$', 'management.views.goods_list'),
    (r'^goods/list/(?P<p_cid>\d+)?$', 'management.views.goods_list'),
    (r'^trans/list/(?P<p_cid>\d+)?$', 'management.views.trans_list'),
    (r'^assign/list/?$', 'management.views.assign_list'),
    (r'^assign/set_cate/?$', 'management.views.goods_set_cate'),
    (r'^goods_set_deleted/?$', 'management.views.goods_set_deleted'),
    (r'^goods_edit/?$', 'management.views.goods_edit'),
    (r'^cate_list/?$', 'management.views.cate_list'),
)
