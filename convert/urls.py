#coding:utf-8
from django.conf.urls import patterns, url

urlpatterns = patterns('',
    (r'^list/?$', 'convert.views.goods_list'),
    (r'^update/(?P<tb_id>\d+)?$', 'convert.views.goods_update'),
    (r'^delete/(?P<tb_id>\d+)?$', 'convert.views.goods_delete'),
)
