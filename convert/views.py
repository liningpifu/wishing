from django.shortcuts import render
from wishing import models as wishing_models
from django.http import HttpResponse
from django.core.serializers.json import DjangoJSONEncoder
import json, random, urllib, time, datetime

from django.views.decorators.csrf import csrf_exempt

def goods_list(request):
    source_id = int(request.GET.get('source_id', '0'))
    goods_list_qs = wishing_models.Goods.objects.filter(cate_id__gt = 0, is_onsale = 0, is_deleted = 0)

    if source_id > 0:
        goods_list_qs = goods_list_qs.filter(source = source_id)

    goods_list_qs = goods_list_qs[:30]

    params = {
            "goods_list":[ g.t_id for g in goods_list_qs],
            }
    return HttpResponse(json.dumps(params), content_type = 'application/json')

@csrf_exempt
def goods_update(request, tb_id):
    detail_url = request.POST.get("detail_url")
    try:
        goods_obj = wishing_models.Goods.objects.get(t_id = tb_id) 
        goods_obj.detail_url = detail_url
        goods_obj.date_onsale = datetime.datetime.now()
        goods_obj.is_onsale = 1
        goods_obj.save()
        params = {"status":True}
    except wishing_models.Goods.DoesNotExist, e:
        params = {"status":False}
    return HttpResponse(json.dumps(params), content_type = 'application/json')

@csrf_exempt
def goods_delete(request, tb_id):
    try:
        goods_obj = wishing_models.Goods.objects.get(t_id = tb_id) 
        goods_obj.is_deleted = 1
        goods_obj.is_onsale = 0
        goods_obj.save()
        params = {"status":True}
    except wishing_models.Goods.DoesNotExist, e:
        params = {"status":False}
    return HttpResponse(json.dumps(params), content_type = 'application/json')
