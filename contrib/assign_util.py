#coding:utf-8
from wishing import models;

def get_cate_t(cate_t, name):
    if cate_t[0] != 0 and cate_t[1] != 0:
        return cate_t
    cate_pid = cate_t[0]
    for cate_item in models.CATE_T:
        if cate_pid != 0:
            #当指定了大类别
            if cate_pid == cate_item[2]:
                keywords = cate_item[3]
                keyword_list = keywords.split(',')
                for keyword in keyword_list:
                    if keyword in name:
                        return (cate_item[2], cate_item[0])
        else:
            #未指定类别
            keywords = cate_item[3]
            keyword_list = keywords.split(',')
            for keyword in keyword_list:
                if keyword in name:
                    return (cate_item[2], cate_item[0])


