 #coding:utf-8

from django.db import models
import datetime, time
from decimal import *

# Create your models here.

P_CATE_CHOICES = (
    (15, u"个性装饰"),
    (1, u"配饰"),
    (2, u"家居"),
    (3, u"办公"),
    (4, u"数码"),
    (5, u"美食"),
    (6, u"洗护"),
    (7, u"彩妆"),
    (8, u"箱包"),
    (9, u"女装"),
    (10, u"女鞋"),
    (11, u"男装"),
    (12, u"男鞋"),
    (13, u"母婴"),
    (14, u"运动"),
)

CATE_T = (
    (100, u"手饰", 1, u"手链,脚链,戒指,手串,手绳,手镯,手环"),
    (101, u"项链", 1, u"项链,锁骨链,毛衣链"),
    (102, u"耳饰", 1, u"耳饰,耳钉,耳环,耳坠,耳钩,耳夹,耳扣,耳罩"),
    (103, u"发饰", 1, u"发饰,头饰,发圈,发箍,发带,发绳,面纱,头纱,头花,边夹,一字夹,发夹,发簪,头绳,发卡"),
    (104, u"帽子", 1, u"帽子,毛线帽,礼帽,画家帽,贝雷帽,滑板帽,棒球帽,草帽,渔夫帽"
            +u",遮阳帽,鸭舌帽,女巫帽,爵士帽,南瓜帽,圆顶帽,嘻哈帽,平沿帽"),
    (105, u"围巾", 1, u"围巾,披肩,方巾,丝巾,围脖,口罩,耳罩,耳套,肩毯"),
    (106, u"眼镜", 1, u"眼镜,镜框,平光镜,墨镜,护目镜,太阳镜,蛤蟆镜,眼镜盒"),
    (107, u"手表", 1, u"手表,电子表,石英表,机械表,腕表,时装表,钢带表,指针表,情侣表,插画表,男表,女表,怀表"),
    (108, u"手套", 1, u"手套,套袖,袖套"),
    (109, u"袜子/鞋垫", 1, u"袜子,过膝袜,绒袜,地板袜,堆堆袜,短袜,长袜,船袜,长筒袜,连裤袜,丝袜,棉袜,打底袜,中筒袜,五指袜"),
    (110, u"领带/腰带", 1, u"领带,腰带,胸针,领结,皮带,领饰,假领,袖钉"),

    (120, u"床上用品", 2, u"床品,三件套,四件套,床罩,毯子,毛毯,盖毯,蚊帐,帐篷,床垫,躺床"),
    (121, u"小家电", 2, u"养生壶,加湿器,咖啡机,净水器,制氧机,空气净化器,除螨器,除湿机,吸尘器,音响"+
            u",豆浆机,打蛋机,面包机,酸奶机,风扇,热水壶,挂烫机,电炖锅,碎肉机,按摩器,冰箱,足浴盆"+
            u",香薰机,烧烤炉,灭蚊灯,洗衣机,消毒机,去皮器,落地扇,遥控,体重秤"+
            u",榨汁机,机器人,取暖器,电饭煲,冰淇淋机"),
    (122, u"收纳", 2, u"收纳,纸巾盒,挂钩,衣钩,抽屉,整理盒,置物,储物,鞋柜,鞋架,格子柜"+
            u",首饰盒,保鲜盒,糖盒,密封夹,书柜,书架,编织袋,红酒架,衣帽架,衣饰钩,衣架,衣柜"),
    (123, u"装饰", 2, u"地垫,脚垫,窗帘,桌布,灯,红包,花架,壁挂,桌椅,茶几,桌,装饰柜,墙饰,梳妆镜,屏风"),
    (124, u"厨具", 2, u"厨房,厨具,刷子,切碎器,碎溶器,过滤器,磨姜器,削皮器,油瓶,烤箱,淘米,调味瓶"+
            u",勺,锅,刀,壶,罐,料理机,切菜板,煎蛋器,剪刀,菜板,洗米"),
    (125, u"餐具", 2, u"餐具,盘,筷子,碗,碟,餐桌布,垫,叉,便当盒"),
    (126, u"杯子/小勺", 2, u"杯,茶具"),
    (127, u"宠物", 2, u"宠物,猫,狗,仓鼠,鱼,窝,牵引绳,坐便器,止吠器,食盆,水族箱,铃铛,项圈"),
    (128, u"百货日杂", 2, u"生活"),
    (129, u"抱枕/坐垫", 2, u"飘窗垫,枕,坐垫,榻榻米,沙发,躺椅,椅"),
    (130, u"毛巾/卫浴", 2, u"毛巾,手帕,浴巾,手绢,干发,干发帽,肥皂盒,马桶垫,漱口杯"),

    (140, u"办公收纳", 3, u"插排,首饰架,首饰盒,桌面,书本,留言板,笔袋,笔筒,计算器,书立,眼镜盒,长尾夹,文件袋,书挡,文具盒,文件框"),
    (141, u"纸/笔", 3, u"本,活页,便条,便利贴,书写板,便签,明信片"+
            u",书签,涂色本,彩铅,笔,墨水,替芯,卡贴,冰箱贴,尺子,橡皮,修正带,卷笔刀"),
    (142, u"桌上物品", 3, u"地球仪,练字,文房,台历,万年历,日历,相册,计划表,装订机,点钞机,扫描枪"),
    (143, u"手账", 3, u"手账,褶皱纸,贴纸,手账本,印章,字母贴,胶带,纪念册,回形针,同学录,信笺,信纸,点点胶,修饰带"),
    (144, u"卡包", 3, u"名片夹,名片盒,卡包,护照夹,钥匙扣,卡套,证件夹"),

    (150, u"摄影周边", 4, u"拍立得,相机,自拍,镜头,灯箱,腰包,肩带,背带,自拍杆,腕带,底座,摄影包,三脚架,胶卷,打印机"),
    (151, u"移动电源", 4, u"移动电源,充电宝"),
    (152, u"手机周边", 4, u"手机,防尘塞,转换插头,充电器,数据线,手机镜头,插座,手机架,支架,防水套,保护皮套,电话筒,扬声器"),
    (153, u"手机壳", 4, u"手机壳,保护套"),
    (154, u"电脑周边", 4, u"鼠标,散热,随身wifi,USB,U盘,硬盘,键盘,内胆包,游戏,导航,笔记本贴纸"+
            u",笔记本保护套,投影仪,打印机,清洗套装,体感,绕线器,分线器,路由"),
    (155, u"耳机音响", 4, u"耳机,音响,耳麦"),

    (160, u"糖果糕点", 5, u"糖,蛋糕,糕点,麻薯,打糕,马卡龙,月饼,果冻,乳酪,三明治,桂花糕"+
            u",绿豆糕,羊羹,糯米滋,芝士,酥,甜甜圈,布丁,麻花"),
    (161, u"咖啡/冲饮", 5, u"咖啡,方糖,蜜露,茶,奶茶,蜂蜜,汤膏,果汁粉"),
    (162, u"坚果/果脯", 5, u"梅干,枣干,蜜饯,坚果,果脯,草莓干,酱,话梅,沙拉酱,鱼子酱,山楂条,柠檬干"+
            u",果干,葡萄干,枣,枸杞,榴莲干,椰子片,芒果干,果肉,豆,碧根果,松子,核桃,花生"),
    (163, u"膨化食品", 5, u"膨化,薯条,米饼,小小酥,虾片,薯片,年糕条,薄片"),
    (164, u"巧克力", 5, u"巧克力"),
    (165, u"代餐", 5, u"代餐,麦片,核桃粉,山药粉,茯苓粉,木瓜粉,葛根粉,黑米圈,通心粉,蛋白质,酵素,玉米片,养生膏,能量棒,藕粉"),
    (166, u"速食/即食", 5, u"手工水饺,馒头,春卷,蘑菇汤,油条,牛蹄筋,肉干,素食"),
    (167, u"曲奇/饼干", 5, u"饼干,曲奇,鲜花饼,面包干,威化饼干,蛋卷,爆米花,麦丽素,奥利奥,烤饼,脆片,老婆饼"),
    (168, u"饮料/雪糕", 5, u"牛奶,饮品,饮料,酸奶,汁,汽水,水,可乐,汁,钙奶,龟苓膏,吸吸冰,碎冰冰 "),
    (169, u"保健食品",  5, u""),

    (170, u"洁面", 6, u"洁面,洗面,洁肤,美容巾,手工皂,精华皂"),
    (171, u"水乳", 6, u"水,乳液,霜,原液,保湿液,精华液,美容液"),
    (172, u"面膜", 6, u"面膜,凝胶,芦荟胶"),
    (173, u"头发护理", 6, u"洗发水,洗发乳,护发素,发膜,发泥,发胶,蓬蓬粉,定型,梳"
            +u",喷雾,弹力素,染发膏,卷发棒,夹板,吹风机,发卷,发粉"),
    (174, u"手部护理", 6, u"护手霜,手膜"),
    (175, u"唇部护理", 6, u"唇膏,唇彩,口红,唇蜜,唇膜,唇球"),
    (176, u"防晒/修复", 6, u" 防晒,修护,晒后"),
    (177, u"面部护理", 6, u"眼,睫毛,鼻,眉毛,美容棒,黑头,角质"),
    (178, u"身体护理", 6, u"足膜,发粉,身体,塑身,沐浴,润体,腋下,手肘,颈,浴盐,脚,足,磨砂,刮痧,搓澡巾,按摩,臀,脱毛"),
    (179, u"护肤套装", 6, u"护肤套装,净白套装,水乳套装,保湿套装,美白套装,补水套装,旅行套装"),
    (180, u"女士香水", 6, u"香氛,香体,女士香水,香水"),
    (181, u"男士香水", 6, u"男士香水"),
    (182, u"男士护肤", 6, u"男士护肤"),
    (183, u"口腔相关", 6, u"牙,漱口,口气,口腔,洁齿,电动牙刷"),
    (184, u"眼部相关", 6, u"眼罩,眼部,眼药水,护眼,洗眼液,滴眼,眼膜"),
    (185, u"女性护理", 6, u"卫生棉条,湿巾,女性护理,经期,痛经,姨妈皂,卫生巾,姨妈巾"
            +u",女性清洁,暖宝宝,暖宫,卫生护垫,姨妈垫"),

    (190, u"眼妆", 7, u"笔，睫毛膏,眼影,眉粉,染眉膏,眼线,眼部卸妆,眉胶,眼彩,卧蚕笔,眼皮贴,假睫毛,修眉"),
    (191, u"口红唇彩", 7, u"口红,唇彩,唇膏,唇蜜,唇液,唇笔"),
    (192, u"美甲/美发", 7, u"指甲,美甲,卸甲，"),
    (193, u"腮红", 7, u"腮红,修容,唇颊膏,胭脂 "),
    (194, u"底妆", 7, u"粉饼,散粉,粉底液,BB霜,CC霜,气垫,蜜粉,遮瑕霜,打底,隔离,妆前,修容"),
    (195, u"美容工具", 7, u"镜,清洁线,化妆刷,直板夹,睫毛夹,棉签,鼻梁增高器,卷发筒,美颜棒,修眉刀,眉毛夹"+
            u",眼影刷,高光刷,腮红刷子,平头刷,化妆棉,卸妆棉,粉扑,吸油,喷雾瓶,美甲工具,祛痘仪,首饰盒,清洁器,美容仪"),
    (196, u"套装", 7, u"彩妆套装,唇彩套装,眼妆套装,明星套装,底妆套装,眼影盘,修容盘"),
    (197, u"其他美妆", 7, u"假发,刘海,接发片,高光,提亮,散粉,纹身贴"),

    (210, u"女士钱包", 8, u"钱包,钱夹"),
    (211, u"背包", 8, u"背包,双肩包,旅行包,单肩包"),
    (212, u"手提包", 8, u"手提包,链条包,贝壳包,鲶鱼包,杀手包,手拎包,方包,挎包,邮差包,帆布包,医生包,公文包,罗马包,鳄鱼包,沙滩包,水桶包,凯莉包"),
    (213, u"手拿包", 8, u"手拿包,护照包,手抓包,手机袋,信封包,斜挎包,小Q包,三角包,手包"),
    (214, u"化妆包", 8, u"化妆包,洗漱包"),
    (215, u"行李箱", 8, u"行李箱,旅行箱,拉杆箱,登机箱,标签牌"),
    (216, u"男士钱包", 8, u"钱包,钱夹"),

    (220, u"T恤", 9, u"T恤"),
    (221, u"衬衫", 9, u"衬衫,雪纺衫,蕾丝衫,蝙蝠衫,娃娃衫,衬衣"),
    (222, u"裙装", 9, u"半身裙,连衣裙,长裙,半裙,短裙,包臀裙,牛仔裙,半身裙,A字裙,A裙,伞裙"+
            u",百褶裙,摆裙,流苏裙,针织裙,背带裙,卫衣裙,衬衫裙,背心裙,短裤裙"),
    (223, u"外套", 9, u"外套,风衣,开衫,马甲"),
    (224, u"内衣", 9, u"内衣,文胸,内裤,生理裤,抹胸,bra,塑身衣"),
    (225, u"针织衫", 9, u"针织衫,毛衣,打底衫"),
    (226, u"长裤", 9, u"长裤,哈伦裤,连衣裤,牛仔裤,阔腿裤,阔脚裤,小脚裤,休闲裤,紧身裤,皮裤"+
            u",毛呢裤,工装裤,西装裤,西裤,背带裤,打底裤,高腰裤,休闲裤,八分裤,九分裤,格子裤,喇叭裤,吊带裤,直筒裤"),
    (227, u"短裤", 9, u"短裤,裙裤,中裤"),
    (228, u"卫衣", 9, u"卫衣"),
    (229, u"情侣装", 9, u"情侣"),
    (230, u"套装", 9, u"套装,连体,两件套"),
    (231, u"家居服", 9, u"家居服,睡衣,睡袍,睡裙,居家,瑜伽服"),

    (240, u"平底/单鞋", 10, u"厚底鞋,平底鞋,单鞋,英伦鞋,皮鞋"),
    (241, u"凉鞋", 10, u"凉鞋,人字拖,凉拖,罗马鞋,绑带鞋"),
    (242, u"靴子", 10, u"靴子,短靴,马丁靴,骑士靴,皮靴,女靴,雪地靴"),
    (243, u"高跟鞋", 10, u"高跟,尖头"),
    (244, u"休闲鞋", 10, u"休闲鞋,乐福鞋,懒人鞋"),
    (245, u"家居鞋", 10, u"家居,拖鞋,棉拖,室内,雨鞋,雨靴,鞋套"),
    (246, u"运动鞋", 10, u"运动,跑步,慢跑,球鞋,篮球鞋"),

    (250, u"衬衫", 11, u"衬衣,衬衫"),
    (251, u"T恤", 11, u"T恤,背心"),
    (252, u"外套", 11, u"外套,风衣,开衫,马甲"),
    (253, u"内衣", 11, u"内衣,内裤"),
    (254, u"短裤", 11, u"短裤,卫裤"),
    (255, u"长裤", 11, u""),
    (256, u"卫衣", 11, u"卫衣"),
    (257, u"针织衫", 11, u"针织衫,毛衣,套头衫"),

    (270, u"皮鞋", 12, u""),
    (271, u"休闲鞋", 12, u""),
    (272, u"运动鞋", 12, u""),

    (280, u"宝宝用品", 13, u"奶瓶,学步车,婴儿,宝宝,儿童,奶粉,喂药器"),
    (281, u"孕妇用品", 13, u"防辐射服,孕妇,吸奶器,母乳,喂奶枕,乳头保护罩"),
    (282, u"童装", 13, u"分腿睡袋,爬行服,女童,男童"),
    (283, u"益智/玩具", 13, u"点读机,儿童钢琴,手指涂鸦,百科全书,练字帖,积木,拼图"+
            u",娃娃,玩具,遥控车,游戏,溜溜球,弹珠,四驱车,泡泡机,换装贴纸,万花筒,画板"),

    (290, u"户外装备", 14, u"户外,折叠杯,吊床,羽毛球,运动,滑板,轮滑,瑞士军刀"+
            u",登山包,腰包,下降器,健腹器,手电筒,吊灯,望远镜,睡袋,帐篷,点火器,野餐"+
            u",气垫床,炊具,饮水袋,便携,绷带,护腿板,骑行手套"),
    (291, u"运动健身", 14, u"瑜伽,减肥,减肚子,踏步机,锻炼器,拉力带,呼啦圈"+
            u",俯卧撑支架,沙袋,单杠,健腹轮,球拍,跳绳,动感单车,运动巾,训练包"+
            u",护腕,指压板,自行车,电子秤,哑铃,头盔,护肘,平衡球,甩脂"),

    (300, u"模型/玩偶", 15, u""),
    (301, u"装饰/摆件", 15, u"打火机"),
    (302, u"动漫/明星", 15, u""),
    (303, u"毛绒公仔", 15, u""),
)

CATE_CHOICES = ((c[0], c[1]) for c in CATE_T)

#小类别key-value
CATE_NAME_DICT = { c[0] : c[1] for c in CATE_T }
#大类别key-value
PCATE_NAME_DICT = { c[0] : c[1] for c in P_CATE_CHOICES }


CATE_MAP = {}#大类对应子类id的tuple
CATE_T_MAP = {}#大类对应(id, name)的tuple

for cate_t in CATE_T:
    cid = cate_t[0]
    cname = cate_t[1]
    cid_p = cate_t[2]

    if CATE_MAP.has_key(cid_p):
        cate_list = CATE_MAP[cid_p]
    else:
        cate_list = []
        CATE_MAP[cid_p] = cate_list

    cate_list.append(cid)

    if CATE_T_MAP.has_key(cid_p):
        cate_t_list = CATE_T_MAP[cid_p]
    else:
        cate_t_list = []
        CATE_T_MAP[cid_p] = cate_t_list

    cate_t_list.append((cid, cname))

class Goods(models.Model):
    t_id = models.BigIntegerField(default=0)
    title = models.CharField(max_length = 128, default="")
    title_sub = models.CharField(max_length = 256, default="")
    pic_url = models.CharField(max_length = 256, default = "")
    detail_url = models.CharField(max_length = 512, default="")
    source = models.IntegerField(default = 0)
    cate_pid = models.IntegerField(choices=P_CATE_CHOICES, default = 0)
    cate_id = models.IntegerField(choices=CATE_CHOICES, default = 0)

    is_onsale = models.IntegerField(default = 0)
    date_onsale = models.DateTimeField(null = True, blank = True)
    is_deleted = models.IntegerField(default = 0)
    state = models.IntegerField(default = 0)

    price_now = models.DecimalField(max_digits = 10, decimal_places = 2, default=Decimal(0.00))
    price_ori = models.DecimalField(max_digits = 10, decimal_places = 2, default=Decimal(0.00))
    date_created = models.DateTimeField(null = True, blank = True)
    date_updated = models.DateTimeField(null = True, blank = True)

    rage = models.DecimalField(max_digits = 10, decimal_places = 2, default=Decimal(0.00))
    rage_begin = models.DateTimeField(null = True, blank = True)
    rage_end = models.DateTimeField(null = True, blank = True)

    discount = models.DecimalField(max_digits = 10, decimal_places = 2, default=Decimal(0.00))
    discount_begin = models.DateTimeField(null = True, blank = True)
    discount_end = models.DateTimeField(null = True, blank = True)

    tb_cate_id = models.IntegerField(default = 0)
    tb_cate_pid = models.IntegerField(default = 0)
    tb_sell_num = models.IntegerField(default = 0)
    tb_ship_free = models.IntegerField(default = 0)
    tb_is_tm = models.IntegerField(default = 0)#tianmao

    detail = models.CharField(max_length = 512, default="")
    detail_pic = models.CharField(max_length = 256, default="")

    def as_dict(self):
        return {
                "id": self.id,
                "t_id": self.t_id,
                "title": self.title,
                "cate_p_id": self.cate_pid,
                "cate_p_dis": self.get_cate_pid_display(),
                "cate_id": self.cate_id,
                "cate_id_dis": self.get_cate_id_display(),
                "price_now": self.price_now,
                "pic_url": self.pic_url,
                "detail_url": self.detail_url,
                }

    def get_cate_items(self):
        if self.cate_pid == 0:
            return None
        else:
            return CATE_T_MAP.get(self.cate_pid)

